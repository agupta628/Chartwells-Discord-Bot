const Discord = require('discord.js');
const client = new Discord.Client();
const token = require('./auth.json').token;

client.on('ready',() => {
    console.log('I\'m Online\nI\'m Online');
});

// var prefix = "@Daka Updater#3702";
var id = '405338228716077057';
var prefix = "<@" + id + ">";

var yosias_id="390655514096173056"

var menu_keywords = 
    ['breakfast', 
    'lunch', 
    'dinner', 
    'menu',
    'food',
    'grub']

var date_keywords = ['today',
    'tomorrow']

var time_keywords = ['time',
    'when']

client.on('message', message => {
    let args = message.content.split(' ').slice(1);
    var result = args.join(' ');

    if(!message.content.startsWith(prefix)) {
        return;
    }

    if (message.author.bot) return;

    if (message.content.indexOf("ping") !== -1) {
        message.channel.send(`Pong! \`${Date.now() - message.createdTimestamp} ms\``);
    }

    if (message.content.indexOf("Hello") !== -1) {
        message.channel.send("World!");
    }

    if(message.author.id == yosias_id) {
        message.channel.send("For you, Yosias, just a couple of spinach leaves");
    }

    for (var i = 0; i < menu_keywords.length; i++) {
        if (message.content.indexOf(menu_keywords[i]) !== -1) {
            var period_index = i;

            if(i > 2) {
                period_index = -1;
            }

            if (message.content.indexOf(date_keywords[1]) !== -1) {
                message.channel.send("Getting the menu for tomorrow...");

                var currentDate =  new Date();
                currentDate.setDate(currentDate.getDate() + 1);

                get_menu(currentDate.toISOString(), period_index, message);
                return;
            }

            message.channel.send("Getting the menu for today...");

            get_menu(new Date().toISOString(), period_index, message);
            return;
        }
    }
});

function get_menu(date, period, message) {
    var request = require('sync-request')

    var url = "https://www.dineoncampus.com/v1/location/menu.json?date=" + date + "&location_id=5877ad223191a20074d827dc&platform=0&site_id=5751fd2b90975b60e0489294";
    
    var res = request('GET', url)

    response = JSON.parse(res.getBody());

    var min = 0;
    var max = response.menu.periods.length;

    if (period !== -1) {
        min = period;
        max = period + 1;
    }

    for (var i = min; i < max; i++) {
        var period = response.menu.periods[i];
        var period_text = "";

        period_text += "***" + period.name + "***\n";

        for(var j = 0; j < period.categories.length; j++) {
            var loc = period.categories[j];

            period_text += "**" + loc.name + "**\n";

            for(var k = 0; k < loc.items.length; k++) {
                var item = loc.items[k].name;
                var desc = loc.items[k].desc;

                period_text += item + "\n";
            }
        }

        message.channel.send(period_text);
    }

//    console.log("here");
//    console.log(date);
//    console.log(period);
//
//    getJSON(url, function(error, response) {
//        console.log("here4");
//        var min = 0;
//        var max = response.menu.periods.length;
//
//        console.log(min);
//        console.log(max);
//        console.log(period);
//
//        if (period !== -1) {
//            min = period;
//            max = period + 1;
//        }
//
//        console.log(min);
//        console.log(max);
//
//        for (var i = min; i < max; i++) {
//            console.log("here2");
//            var period = response.menu.periods[i];
//
//            console.log("## " + period + "\n");
//
//            for(var j = 0; j < period.categories.length; j++) {
//                var loc = period.categories[j];
//
//                console.log("#### " + loc + "\n");
//
//                for(var k = 0; k < loc.items.length; k++) {
//                    var item = loc.items[k].name;
//                    var desc = loc.items[k].desc;
//
//                    console.log("* " + item + "\n");
//                }
//            }
//        }
//    });
}

client.login(token);
